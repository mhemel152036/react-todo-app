import React, {useContext} from 'react'
import {TodoContext} from '../App'
import ToDoDetails from './ToDoDetails'
import {Link} from 'react-router-dom'

function ToDoListItem(props) {
    const {id, task, description} = props.todoItem;
    const contextObj = useContext(TodoContext);
    const {todo, setTodo} = contextObj;

    const removeTaskHandeler = (id) => {
        const filteredArr = todo.filter((todoItem) => todoItem.id !== id);
        setTodo(filteredArr);
    }

    return (
        <li className="list-group-item list-group-item-action">
            <Link to={`/todo-details/${id}`}>{task}</Link>
            {/* {task} */}
            <button 
                className="float-right btn btn-danger btn-sm" 
                style={{marginRight: 10}}
                onClick={(event) => removeTaskHandeler(id)}
            >Delete</button>
        </li>
        
    )
}

export default ToDoListItem
