import React, {useEffect} from 'react'
import {BrowserRouter as Router, Link} from 'react-router-dom'

function ToDoHeader() {
    
    useEffect(() => {
        console.log("Header render.");
    }, []);
    
    return (
        <nav className={`navbar navbar-dark bg-dark`}>
            <div className="container">
                <a className="navbar-brand text-light">
                    ToDo List
                </a>
                <ul class="navbar-nav mr-5">
                    <li class="nav-item">
                        <Link className="nav-link" to="/"> Home </Link>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default ToDoHeader
