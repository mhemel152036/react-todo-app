import React, {useState, useContext, useEffect} from 'react'
import {TodoContext} from '../App.js'

const initalTask = '';
const initialDescription = '';

function ToDoForm() {
    const contextObj = useContext(TodoContext);
    const [task, setTask] = useState(initalTask);
    const [description, setDescription] = useState(initialDescription);
    const {todo, setTodo} = contextObj;

    /* disable unnecessary rendering while changing state */
    useEffect(() => {
        console.log("Todo form rendered..");
    }, []);

    const formSubmitHandeler = (event) => {
        event.preventDefault();
        /* validate empty task and description*/
        if(task && description){
            setTodo([...todo, {id: todo.length, task: task, description: description}]);
        }
        else {
            console.log("Task name or description can't be empty");
            throw new Error("Empty Task or Description");
        }
        setTask(initalTask); /* clear task input field */
        setDescription(initialDescription); /* clear description field */
    }
    return (
        <React.Fragment>
            <div className="container my-3">
                <div className="row">
                    <div className="col-sm-6 offset-3">
                        <p className="lead">Add Task</p>
                        <form onSubmit={(event) => formSubmitHandeler(event)}>
                            <input 
                                className="form-control form-control-sm" 
                                type="text" 
                                name="task"
                                onChange={(event) => setTask(event.target.value)}
                                value={task}
                                placeholder="Add a new task..." 
                            />
                            <br />
                            <textarea 
                                className="form-control form-control-sm" 
                                rows="3" cols="30" 
                                name="description"
                                onChange={(event) => setDescription(event.target.value)}
                                value={description}
                                placeholder="Add task description..."
                            />
                            <br />
                            <button type="submit" className="float-right btn btn-sm btn-default btn-info">Add Task</button>
                        </form>
                    </div>
                </div>
            </div>
            {/* <TodoContext.Provider value={{todo: todo, setTodo: setTodo}}>
                <ToDOList />
            </TodoContext.Provider> */}
        </React.Fragment>
    )
}

export default ToDoForm
