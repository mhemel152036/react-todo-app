import React, {useState, useEffect} from 'react'
import {useParams, Redirect} from 'react-router-dom'

function ToDoDetails(props) {
    const {todo} = props;
    const {id} = useParams();
    const [task, setTask] = useState('');
    const [description, setDescription] = useState('');

    useEffect(() =>{
        // console.log(id);
        // console.log(todo);
        if(todo.length > 0){
            const todoById = todo.find(todoItem => todoItem.id == id);
            setTask(todoById.task);
            setDescription(todoById.description);
        }
        else{
            console.log("Empty ToDo List.");
        }
    }, [todo, id]);

    const validateTask = () => {
        if(task && description) {
            return (
                <div className="card">
                    <h3 className="card-header">{task}</h3>
                    <div className="card-body lead">
                        {description}
                    </div>
                </div>
            );
        }
        else{
            return <h1 className="text-danger text-center my-5">Not Found</h1>;
        }
    }
    return (
        <div className="container my-5">
            <div className="row">
                <div className="col-sm-6 offset-3">
                    {validateTask()}
                </div>
            </div>
        </div>
    )
}

export default ToDoDetails
