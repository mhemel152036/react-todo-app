import React, {useContext, useEffect} from 'react'
import ToDoListItem from './ToDoListItem'
import {TodoContext} from '../App'

function ToDOList(props) {
    const contextObj = useContext(TodoContext);
    const {todo, setTodo} = contextObj;

    useEffect(() => {
        console.log("ToDo list render.");
    },[]);
    
    return (
        <div className="container">
            <div className="row">
                <div className="col-sm-6 offset-3">
                <p className="lead mb-0">Task List</p>
                <hr />
                    <ul className="list-group">
                        {
                            todo.map((todoItem) => {
                                return <ToDoListItem key={todoItem.id} todoItem={todoItem} />
                            })
                        }
                    </ul>
                </div>
            </div>
            
        </div>
    )
}

export default ToDOList
