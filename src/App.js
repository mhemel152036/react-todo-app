import React, {useState} from 'react'
import ToDoForm from './Components/ToDoForm'
import ToDoHeader from './Components/ToDoHeader'
import ToDoList from './Components/ToDOList'
import ToDoDetails from './Components/ToDoDetails'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

export const TodoContext = React.createContext();
const initialTodo = [];

function App() {
  const [todo, setTodo] = useState(initialTodo);
  return (
    <React.Fragment>
      <Router>
      <ToDoHeader />
        <Route exact path="/">
          <TodoContext.Provider value={{todo: todo, setTodo: setTodo}}>
            <ToDoForm />
            <ToDoList />
          </TodoContext.Provider>
        </Route>
        <Route path="/todo-details/:id">
          <ToDoDetails todo={todo} />
        </Route>
      </Router>
    </React.Fragment>
  );
}

export default App;
